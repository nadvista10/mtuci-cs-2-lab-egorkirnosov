# Problem Set 2, hangman.py
# Name: Egor
# Collaborators: -
# Time spent: 2h
# Hangman Game
# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)
import random
import string
import re

from constants import *

WORDLIST_FILENAME = "words.txt"

def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    global words_line
    words_line = ' ' + inFile.readline() + ' ' # пробел нужен для нормальной работы regex
    # wordlist: list of strings
    wordlist = words_line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist

def get_unique_letters_count(word):
    letters = []
    for letter in word:
        if (not letter in letters):
            letters.append(letter)
    return(len(letters))

def is_letter_guessed(secret_word, letter):
    return letter in secret_word

def check_input(letters_available, input):
    return ((len(input) == 1) and (input in alphabet_eng)),(input in letters_available)

def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)
    
    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code

# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = load_words()


def is_word_guessed(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if all the letters of secret_word are in letters_guessed;
      False otherwise
    '''

    for letter in secret_word:
        if(not letter in letters_guessed):
            return False
    return True


def get_guessed_word(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string, comprised of letters, underscores (_), and spaces that represents
      which letters in secret_word have been guessed so far.
    '''
    answer = ''
    for letter in secret_word:
        if(letter in letters_guessed):
            answer += letter
        else:
            answer += unknown_letter_smb + ' '
    return answer



def get_available_letters(letters_guessed):
    '''
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string (of letters), comprised of letters that represents which letters have not
      yet been guessed.
    '''
    answer = alphabet_eng
    for letter in letters_guessed:
        answer = answer.replace(letter,'')
    return answer
    
    

def hangman(secret_word, allow_hints = False):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses

    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Remember to make
      sure that the user puts in a letter!
    
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
    
    Follows the other limitations detailed in the problem write-up.
    '''


    #     greeting and initializing game variables
    guesses_left = start_guesses_count
    warnings_left = start_warnings_count

    print('Welcome to the game Hangman!')
    print('I am thinking of a word that is {} letters long.'.format(len(secret_word)))
    print('You have {0} warnings left.'.format(warnings_left))
    letters_used = []
    letters_guessed = []

    #     game loop
    while(guesses_left >= 0 and warnings_left >= 0):

        if(is_word_guessed(secret_word,letters_guessed)):
            print('Congratulations, you won!')
            break

        print(line_separator)

        currently_available_letters = get_available_letters(letters_used)
        print('You have {} guesses left'.format(guesses_left))
        print('Available letters: {}'.format(currently_available_letters))

        user_answer = input('Please guess a letter: ').lower()

        #     hints logic
        if(allow_hints and user_answer == hint_symbol):
            word = get_guessed_word(secret_word,letters_guessed)
            hints = get_possible_matches(word)
            print('Possible word matches are: ')
            print(''.join(hints))
            continue

        #     error notification
        is_letter_available,is_letter_not_guessed = check_input(currently_available_letters,user_answer)
        if (not is_letter_available):
            warnings_left -= 1
            print('Oops! That is not a valid letter. You have {0} warnings left: {1}'.format(warnings_left, get_guessed_word(secret_word,letters_guessed)))
            continue
        if (not is_letter_not_guessed):
            warnings_left -= 1
            print('Oops! Youve already guessed that letter.  You have {0} warnings left: {1}'.format(warnings_left, get_guessed_word(secret_word,letters_guessed)))
            continue

        #     checking the entered letter
        letters_used.append(user_answer)
        if(is_letter_guessed(secret_word,user_answer)):

           letters_guessed.append(user_answer)

           print('Good guess: {}'.format(get_guessed_word(secret_word,letters_guessed)))
        else:
            print('Oops! That letter is not in my word: {}'.format(get_guessed_word(secret_word,letters_guessed)))
            if (user_answer in vowels_eng):
                guesses_left -= vowels_mulct
            else:
                guesses_left -= consonants_mulct

    #     game over
    if(guesses_left < 0):
        print(line_separator)
        print('Sorry, you ran out of guesses. The word was {}.'.format(secret_word))
    elif(warnings_left < 0):
        print(line_separator)
        print('Sorry, you ran out of warnings. The word was {}.'.format(secret_word))
    else:
        total_score = guesses_left * get_unique_letters_count(secret_word)
        print(total_score)


def get_possible_matches(my_word):
    '''
    my_word: string with _ characters, current guess of secret word
    returns: nothing, but should print out every word in wordlist that matches my_word
             Keep in mind that in hangman when a letter is guessed, all the positions
             at which that letter occurs in the secret word are revealed.
             Therefore, the hidden letter(_ ) cannot be one of the letters in the word
             that has already been revealed.

    '''

    regex_template = r"\b"
    for letter in my_word.replace(' ',''):
        if (letter == unknown_letter_smb):
            regex_template += r"\w"
        else:
            regex_template += letter
    regex_template += r"\s{1}"
    regex = re.compile(regex_template)

    matches = regex.findall(words_line)
    return matches



if __name__ == "__main__":
    secret_word = choose_word(wordlist)
    hangman(secret_word,True)

