import string

WORDLIST_FILENAME = "words.txt"
unknown_letter_smb = '_'
alphabet_eng = string.ascii_lowercase
vowels_eng = 'aeiou'
start_guesses_count = 6
line_separator = '------------'
start_warnings_count = 3
hint_symbol = '*'

vowels_mulct = 2
consonants_mulct = 1
